package grails.rest

class BoardController extends RestfulController {
    static responseFormats = ['json', 'xml']

    BoardController() {
        super(Board)
    }
}
